package at.gv.egiz.pdfas.lib.api.timestamp;

import java.io.IOException;

import javax.annotation.Nonnull;

/**
 * Interface for a service that creates and adds a RFC 3161 timestamp token to an existing CMS ContentInfo.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 */
@FunctionalInterface
public interface CMSTimeStamper {

	/**
	 * Creates and adds a RFC 3161 TimeStampToken using the signature value of the provided {@code cmsContentInfo}.
	 * 
	 * @param cmsContentInfo The ASN.1 encoded CMS ContentInfo. (required; must not be {@code null})
	 * @return The extended CMS ContentInfo. (never {@code null})
	 * @throws IOException In case of IO error, e.g. when communicating with a timestamping authority.
	 * @apiNote The CMS ContentInfo is expected to contain a single SignerInfo.
	 */
	@Nonnull
	byte[] addTimeStamp(@Nonnull byte[] cmsContentInfo) throws IOException;

}
