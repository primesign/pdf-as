package at.gv.egiz.pdfas.lib.api.timestamp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import iaik.asn1.ASN1Object;
import iaik.asn1.CodingException;
import iaik.asn1.DerCoder;
import iaik.asn1.ObjectID;
import iaik.asn1.structures.Attribute;
import iaik.cms.CMSException;
import iaik.cms.ContentInfo;
import iaik.cms.SignedData;
import iaik.cms.SignerInfo;

/**
 * Implementation of a service for adding a signature time stamp to a CMS SignerInfo (wrapped in a CMS ContentInfo). The
 * ContentInfo is expected to contain exactly one single SignerInfo. The service requires a {@link TimeStamper}
 * implementation that creates RFC 3161 related timestamps for certain signature values.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@ThreadSafe
@Immutable
public class CMSTimeStamperImpl implements CMSTimeStamper {

	private Logger log = LoggerFactory.getLogger(CMSTimeStamperImpl.class);

	private final TimeStamper timeStamper;

	/**
	 * Creates a new instance.
	 * 
	 * @param timeStamper The RFC 3161 related time stamping service. (required; must not be {@code null}
	 * @apiNote The timestamper must bt thread-safe.
	 */
	public CMSTimeStamperImpl(@Nonnull TimeStamper timeStamper) {
		this.timeStamper = Objects.requireNonNull(timeStamper, "'timeStamper' must not be null.");
	}

	@Override
	public byte[] addTimeStamp(byte[] cmsContentInfo) throws IOException {

		try (InputStream in = new ByteArrayInputStream(cmsContentInfo)) {

			ContentInfo contentInfo = new ContentInfo(in);

			if (!contentInfo.getContentType().equals(ObjectID.cms_signedData)) {
				throw new IllegalArgumentException("ContentInfo does not contain signed data.");
			}

			try (InputStream cIn = contentInfo.getContentInputStream()) {
				SignedData signedData = new SignedData(cIn);

				SignerInfo[] signerInfos = signedData.getSignerInfos();
				if (signerInfos == null || signerInfos.length != 1) {
					throw new IllegalArgumentException("Exactly one single signer info expected.");
				}

				SignerInfo signerInfo = signerInfos[0];
				byte[] timeStampToken = timeStamper.applyTimeStamp(signerInfo.getSignatureValue());
				if (log.isTraceEnabled()) {
					log.trace("Raw timestamp token: {}", Hex.encodeHexString(timeStampToken));
				}

				// add timestamp token (even if the authority that created the cms already includes a timestamp)
				addTimeStampToken(signerInfo, timeStampToken);

				contentInfo.setContent(signedData);
				return contentInfo.getEncoded();

			}
			
		} catch (CMSException e) {
			throw new IllegalArgumentException("Unable to parse the provided cms ContentInfo.", e);
			
		} catch (CodingException e) {
			throw new IllegalStateException("Unable to decode the timestamp token from timestamping authority.", e);
		}

	}

	/**
	 * Adds the encoded {@code timeStampToken} to the provided {@code signerInfo} as unsigned attribute.
	 * 
	 * @param signerInfo     The signer info. (required; must not be {@code null})
	 * @param timeStampToken The encoded {@code timeStampToken} to be added. (required; must not be {@code null})
	 * @throws CodingException Thrown in case of error decoding the provided {@code timeStampToken}.
	 */
	private void addTimeStampToken(@Nonnull SignerInfo signerInfo, @Nonnull byte[] timeStampToken) throws CodingException {

		ASN1Object asn1TimeStampToken = DerCoder.decode(timeStampToken);
		Attribute timeStampAttribute = new Attribute(ObjectID.timeStampToken, new ASN1Object[] { asn1TimeStampToken });
		log.info("Adding signature timestamp (as unsigned attribute) to cms signer info.");
		signerInfo.addUnSignedAttribute(timeStampAttribute);

	}

}
