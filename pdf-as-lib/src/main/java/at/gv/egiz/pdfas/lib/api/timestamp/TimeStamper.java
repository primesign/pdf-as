package at.gv.egiz.pdfas.lib.api.timestamp;

import java.io.IOException;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Interface for a service that creates TimeStampTokens based on given signature values according to RFC 3161.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 * @see <a href="https://www.ietf.org/rfc/rfc3161.txt">RFC 3161: Time-Stamp Protocol (TSP)</a>
 * @apiNote The respective implementation is required to validate the received timestamp token according to RFC 3161.
 */
@FunctionalInterface
@ThreadSafe
public interface TimeStamper {

	/**
	 * Creates a RFC 3161 TimeStampToken using the provided {@code signatureValue}.
	 * 
	 * @param signatureValue The signature value. (required; must not be {@code null})
	 * @return The TimeStampToken. (never {@code null})
	 * @throws IOException In case of IO error, e.g. when communicating with a timestamping authority.
	 */
	@Nonnull
	byte[] applyTimeStamp(@Nonnull byte[] signatureValue) throws IOException;

}
