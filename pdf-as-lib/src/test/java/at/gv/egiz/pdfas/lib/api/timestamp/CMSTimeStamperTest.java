package at.gv.egiz.pdfas.lib.api.timestamp;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.expect;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Test;

import iaik.asn1.CodingException;
import iaik.asn1.DerCoder;
import iaik.asn1.ObjectID;
import iaik.asn1.PrintableString;
import iaik.asn1.structures.Attribute;
import iaik.cms.CMSParsingException;
import iaik.cms.ContentInfo;
import iaik.cms.SignedData;

public class CMSTimeStamperTest {
	
	private final IMocksControl ctrl = EasyMock.createControl();
	
	private final TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
	
	private final CMSTimeStamper cut = new CMSTimeStamperImpl(timeStamper);
	
	@Test
	public void test_addTimeStamp() throws DecoderException, IOException, CodingException, CMSParsingException {
		
		byte[] encodedCMSContentInfo;
		try (InputStream in = CMSTimeStamperTest.class.getResourceAsStream("cmsSignerInfo.hex")) {
			String hexString = IOUtils.toString(in, StandardCharsets.UTF_8);
			encodedCMSContentInfo = Hex.decodeHex(hexString);
		}
		
		ctrl.reset();
		
		// simulate timestamp with a PrintableString (timestamp validation is intended to be conducted by the timestamper implementation which is mocked here)
		PrintableString timeStampToken = new PrintableString("I'm a timestamp");
		byte[] encodedTimeStampToken = DerCoder.encode(timeStampToken);
		expect(timeStamper.applyTimeStamp(anyObject())).andReturn(encodedTimeStampToken);
		
		ctrl.replay();
		
		byte[] updatedCMSContentInfo = cut.addTimeStamp(encodedCMSContentInfo);
		
		ctrl.verify();

		// parse ContentInfo and look for unsigned timeStampTokenAttribute
		
		ContentInfo contentInfo = new ContentInfo(DerCoder.decode(updatedCMSContentInfo));
		
		// look at the first SignerInfo and make sure the timeStampToken has been added as unsigned attribute
		try (InputStream in = contentInfo.getContentInputStream()) {
			SignedData signedData = new SignedData(in);
			Attribute unsignedAttribute = signedData.getSignerInfos()[0].getUnsignedAttribute(new ObjectID("1.2.840.113549.1.9.16.2.14"));
			assertThat(unsignedAttribute.getValue()[0], is(timeStampToken));
		}
		
	}

}
