package at.gv.egiz.pdfas.lib.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.activation.DataSource;
import javax.annotation.Nonnull;
import javax.annotation.WillNotClose;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.HexDump;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.easymock.Capture;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Test;

import at.gv.egiz.pdfas.common.exceptions.PDFASError;
import at.gv.egiz.pdfas.common.exceptions.PdfAsException;
import at.gv.egiz.pdfas.lib.api.ByteArrayDataSource;
import at.gv.egiz.pdfas.lib.api.Configuration;
import at.gv.egiz.pdfas.lib.api.PdfAs;
import at.gv.egiz.pdfas.lib.api.PdfAsFactory;
import at.gv.egiz.pdfas.lib.api.SignaturePosition;
import at.gv.egiz.pdfas.lib.api.StatusRequest;
import at.gv.egiz.pdfas.lib.api.sign.ExternalSignatureContext;
import at.gv.egiz.pdfas.lib.api.sign.SignParameter;
import at.gv.egiz.pdfas.lib.api.sign.SignParameter.TSMode;
import at.gv.egiz.pdfas.lib.api.sign.SignResult;
import at.gv.egiz.pdfas.lib.api.sign.SigningTimeSource;
import at.gv.egiz.pdfas.lib.api.timestamp.TimeStamper;
import at.gv.egiz.pdfas.lib.impl.status.OperationStatus;
import at.gv.egiz.pdfas.lib.impl.status.RequestedSignature;
import at.gv.egiz.pdfas.sigs.pades.PAdESExternalSigner;
import at.gv.egiz.pdfas.sigs.pades.PAdESSignerKeystore;
import iaik.asn1.DerCoder;
import iaik.asn1.ObjectID;
import iaik.asn1.PrintableString;
import iaik.asn1.structures.AlgorithmID;
import iaik.cms.InvalidSignatureValueException;

public class PdfAsImplTest {

	private final File pdfasConfigFolder = new File(PdfAsImplTest.class.getResource("/pdfas-config").getFile());
	private final PdfAs pdfas = PdfAsFactory.createPdfAs(pdfasConfigFolder);
	
	private final IMocksControl ctrl = EasyMock.createControl();
	
	private final PAdESSignerKeystore signer;
	
	private final DataSource inputDataSource;
	
	private final X509Certificate signingCertificate;
	
	private final PrivateKey signingKey;
	
	public PdfAsImplTest() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, PDFASError, UnrecoverableKeyException {
		
		KeyStore keyStore = KeyStore.getInstance("PKCS12");
		try (InputStream in = PdfAsImplTest.class.getResourceAsStream("/test.p12")) {
			keyStore.load(in, "123456".toCharArray());
		}
		signer = new PAdESSignerKeystore(keyStore, "ecc_test", "123456");
		
		try (InputStream in = getClass().getResourceAsStream("/rotate_090.pdf")) {
			inputDataSource = new ByteArrayDataSource(IOUtils.toByteArray(in));
		}
		
		signingCertificate = (X509Certificate) keyStore.getCertificate("ecc_test");
		signingKey = (PrivateKey) keyStore.getKey("ecc_test", "123456".toCharArray());
		
	}
	
	@Test
	public void test_sign_useOfSigningTimeSource() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		
		ctrl.reset();
		
		SigningTimeSource signingTimeSource = ctrl.createMock(SigningTimeSource.class);
		signParameter.setSigningTimeSource(signingTimeSource);
		
		Calendar otherDay = Calendar.getInstance();
		otherDay.setTime(Date.from(Instant.parse("2007-12-03T10:15:30.00Z")));
		
		expect(signingTimeSource.getSigningTime(anyObject(RequestedSignature.class))).andReturn(otherDay);
		
		ctrl.replay();
		
		SignResult result = pdfas.sign(signParameter);
		
		ctrl.verify();
		
		assertThat(result.getSigningDate(), is(otherDay));
		
	}
	
	@Test
	public void test_sign_tsMode_default_withTimeStamper() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		
		// assert default mode is NONE
		assertThat(signParameter.getTSMode(), is(TSMode.NONE));
		
		ctrl.reset();
		
		// timestamper should be ignored
		TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
		signParameter.setTimeStamper(timeStamper);
		
		ctrl.replay();
		
		SignResult result = pdfas.sign(signParameter);
		
		ctrl.verify(); // expect that timeStamper has not been invoked
		
		assertFalse(result.isTimeStamped()); // tsMode: NONE
		
	}
	
	@Test
	public void test_sign_tsMode_default_withoutTimeStamper() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		
		// assert default mode is NONE
		assertThat(signParameter.getTSMode(), is(TSMode.NONE));

		ctrl.reset();
		
		ctrl.replay();
		
		SignResult result = pdfas.sign(signParameter); // expect no problem without any timestamper
		
		ctrl.verify();
		
		assertFalse(result.isTimeStamped()); // tsMode: NONE
		
	}
	
	@Test
	public void test_sign_tsMode_OPTIONAL_withoutTimeStamper() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		signParameter.setTSMode(TSMode.OPTIONAL);
		
		ctrl.reset();
		
		// makes no sense not to set a timeStamper but this test makes sure signature completes with TSMode.OPTIONAL even with a missing timestamper 
//		TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
//		signParameter.setTimeStamper(timeStamper);
		
		ctrl.replay();
		
		SignResult result = pdfas.sign(signParameter);
		
		ctrl.verify();
		
		assertFalse(result.isTimeStamped()); // no timeStamper
		
	}
	
	@Test
	public void test_sign_tsMode_OPTIONAL_withTimeStamper() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		signParameter.setTSMode(TSMode.OPTIONAL);

		ctrl.reset();
		
		TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
		signParameter.setTimeStamper(timeStamper);
		
		expect(timeStamper.applyTimeStamp(anyObject())).andReturn(DerCoder.encode(new PrintableString("I'm a timestamp"))); // we need a valid ASN.1 object as timestamp otherwise ContentInfo/SignerInfo will not be updated
		
		ctrl.replay();
		
		SignResult result = pdfas.sign(signParameter);
		
		ctrl.verify();
		
		assertTrue(result.isTimeStamped());
		
	}

	@Test
	public void test_sign_tsMode_OPTIONAL_withTimeStamper_failed() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		signParameter.setTSMode(TSMode.OPTIONAL);

		ctrl.reset();
		
		TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
		signParameter.setTimeStamper(timeStamper);

		// test that signature completes with TSMode.OPTIONAL even if timestamping fails
		expect(timeStamper.applyTimeStamp(anyObject())).andThrow(new RuntimeException("timestamping failed"));
		
		ctrl.replay();
		
		SignResult result = pdfas.sign(signParameter);
		
		ctrl.verify();
		
		assertFalse(result.isTimeStamped());
		
	}
	
	@Test
	public void test_sign_tsMode_REQUIRED_withTimeStamper() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		signParameter.setTSMode(TSMode.REQUIRED);
		
		ctrl.reset();
		
		TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
		signParameter.setTimeStamper(timeStamper);
		
		expect(timeStamper.applyTimeStamp(anyObject())).andReturn(DerCoder.encode(new PrintableString("I'm a timestamp"))); // we need a valid ASN.1 object as timestamp otherwise ContentInfo/SignerInfo will not be updated
		
		ctrl.replay();
		
		SignResult result = pdfas.sign(signParameter);
		
		ctrl.verify();
		
		assertTrue(result.isTimeStamped());
		
	}
	
	@Test
	public void test_sign_tsMode_REQUIRED_withoutTimeStamper() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		signParameter.setTSMode(TSMode.REQUIRED);
		
		ctrl.reset();
		
		// no timestamper set
//		TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
//		signParameter.setTimeStamper(timeStamper);
		
		ctrl.replay();
		
		PDFASError error = assertThrows(PDFASError.class, () -> pdfas.sign(signParameter));
		assertThat(error.getCode(), is(11201L)); // ERROR_SIG_TS_TSMODE_REQUIRES_TIMESTAMPER
		
		ctrl.verify();
		
	}
	
	@Test
	public void test_sign_tsMode_REQUIRED_withTimeStamper_failed() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		signParameter.setTSMode(TSMode.REQUIRED);

		ctrl.reset();
		
		TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
		signParameter.setTimeStamper(timeStamper);
		
		expect(timeStamper.applyTimeStamp(anyObject())).andThrow(new RuntimeException("timestamping failed"));
		
		ctrl.replay();
		
		PDFASError error = assertThrows(PDFASError.class, () -> pdfas.sign(signParameter));
		assertThat(error.getCode(), is(11200L)); // ERROR_SIG_TS_FAILED
		
		ctrl.verify();
		
	}
	
	@Test
	public void test_sign_resultProvidesSigningTime() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		
		SignResult result = pdfas.sign(signParameter);
		
		assertThat(result.getSigningDate().getTime()).isInSameMinuteWindowAs(new Date());
		
	}
	
	@Test
	public void test_stepwise_sign_resultProvidesSigningTime() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException, PdfAsException {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		
		StatusRequest statusRequest = pdfas.startSign(signParameter);
		
		statusRequest.setCertificate(signingCertificate.getEncoded());
		statusRequest = pdfas.process(statusRequest);

		// since we need a valid signature value we perform a signature (outside the startSign -> process -> process -> finishSign path).

		ctrl.reset();
		
		RequestedSignature requestedSignature = ctrl.createMock(RequestedSignature.class);
		OperationStatus operationStatus = ctrl.createMock(OperationStatus.class);
		expect(requestedSignature.getStatus()).andReturn(operationStatus);
		expect(operationStatus.getMetaInformations()).andReturn(new HashMap<>());

		ctrl.replay();
		
		byte[] signature = signer.sign(statusRequest.getSignatureData(), statusRequest.getSignatureDataByteRange(), signParameter, requestedSignature);
		
		// now we have a signature value for the next process step
		
		statusRequest.setSigature(signature);
		statusRequest = pdfas.process(statusRequest);
		
		SignResult result = pdfas.finishSign(statusRequest);
		
		assertThat(result.getSigningDate().getTime()).isInSameMinuteWindowAs(new Date());

	}
	
	@Test
	public void test_stepwise_sign_useOfSigningTimeSource() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException, PdfAsException {

		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		
		StatusRequest statusRequest = pdfas.startSign(signParameter);
		
		ctrl.reset();
		
		SigningTimeSource signingTimeSource = ctrl.createMock(SigningTimeSource.class);
		signParameter.setSigningTimeSource(signingTimeSource);
		
		Calendar otherDay = Calendar.getInstance();
		otherDay.setTime(Date.from(Instant.parse("2007-12-03T10:15:30.00Z")));
		
		expect(signingTimeSource.getSigningTime(anyObject(RequestedSignature.class))).andReturn(otherDay);
		
		ctrl.replay();
		
		statusRequest.setCertificate(signingCertificate.getEncoded());
		statusRequest = pdfas.process(statusRequest);
		
		ctrl.verify();
		
		
		// since we need a valid signature value we perform a signature (outside the startSign -> process -> process -> finishSign path).

		ctrl.reset();
		
		RequestedSignature requestedSignature = ctrl.createMock(RequestedSignature.class);
		OperationStatus operationStatus = ctrl.createMock(OperationStatus.class);
		expect(requestedSignature.getStatus()).andReturn(operationStatus);
		expect(operationStatus.getMetaInformations()).andReturn(new HashMap<>());

		ctrl.replay();
		
		byte[] signature = signer.sign(statusRequest.getSignatureData(), statusRequest.getSignatureDataByteRange(), signParameter, requestedSignature);
		
		// now we have a signature value for the next process step
		
		statusRequest.setSigature(signature);
		statusRequest = pdfas.process(statusRequest);
		
		SignResult result = pdfas.finishSign(statusRequest);
		
		assertThat(result.getSigningDate(), is(otherDay));

	}
	
	private static Calendar toCalendar(String isoDateString) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(Date.from(Instant.parse(isoDateString)));
		return calendar;
	}
	
	@Test
	public void test_startExternalSignature() throws PDFASError {
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(new PAdESExternalSigner());
		
		ctrl.reset();
		
		SigningTimeSource signingTimeSource = ctrl.createMock(SigningTimeSource.class);
		signParameter.setSigningTimeSource(signingTimeSource);
		
		Calendar signingTime = toCalendar("2007-12-03T10:15:30.00Z");
		
		expect(signingTimeSource.getSigningTime(anyObject(RequestedSignature.class))).andReturn(signingTime);
		
		ctrl.replay();
		
		ExternalSignatureContext ctx = new ExternalSignatureContext();
		
		pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
		
		ctrl.verify();
		
		assertThat(ctx.getDigestAlgorithmOid(), is(AlgorithmID.sha256.getAlgorithm().getID())); // "2.16.840.1.101.3.4.2.1"
		assertNotNull(ctx.getDigestValue());
		assertNotNull(ctx.getPreparedDocument());
		assertThat(ctx.getSignatureAlgorithmOid(), is(AlgorithmID.ecdsa_With_SHA256.getAlgorithm().getID())); // "1.2.840.10045.4.3.2"
		assertNotNull(ctx.getSignatureByteRange());
		assertNotNull(ctx.getSignatureObject());
		assertThat(ctx.getSigningCertificate(), is(signingCertificate));
		assertThat(ctx.getSigningTime(), is(signingTime));
		
	}
	
	@Test
	public void test_startExternalSignature_finishExternalSignature() throws Exception {

		String signedFileName = getClass().getSimpleName() + "-test_finishExternalSignature-" + System.currentTimeMillis() + ".pdf";
		File signedFile = new File(FileUtils.getTempDirectory(), signedFileName);
		
		try (OutputStream out = new FileOutputStream(signedFile)) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setSignaturePosition("p:1;x:2;y:3");
			signParameter.setSignatureProfileId("SIGNATURBLOCK_DE");
			
			ctrl.reset();
			
			SigningTimeSource signingTimeSource = ctrl.createMock(SigningTimeSource.class);
			signParameter.setSigningTimeSource(signingTimeSource);
			
			Calendar signingTime = toCalendar("2007-12-03T10:15:30.00Z");
			
			expect(signingTimeSource.getSigningTime(anyObject(RequestedSignature.class))).andReturn(signingTime);
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// make sure ctx is updated
			assertNotNull(ctx.getSignaturePosition());
			assertThat(ctx.getSignaturePosition().getPage(), is(1));
			assertThat(ctx.getSignaturePosition().getX(), is(2f));
			assertThat(ctx.getSignaturePosition().getY(), is(3f));
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			SignResult signResult = pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);
			
			ctrl.verify();
			
			assertNotNull(signResult);
			assertThat(signResult.getSignerCertificate(), is(signingCertificate));
			assertThat(signResult.getSigningDate(), is(signingTime));
			assertThat(signResult.getProcessInformations(), hasEntry("SigDevice", "external signature device"));
			
			SignaturePosition signaturePosition = signResult.getSignaturePosition();
			assertNotNull(signaturePosition);
			assertThat(signaturePosition.getPage(), is(1));
			assertThat(signaturePosition.getX(), is(2f));
			assertThat(signaturePosition.getY(), is(3f));
			
			assertFalse(signResult.isTimeStamped());

		} catch (Exception e) {
			signedFile.delete();
			throw e;
		}
		
		System.out.println("Signed file: " + signedFile.getAbsolutePath());

	}
	
	@Test
	public void test_startExternalSignature_finishExternalSignature_tsMode_default_withTimeStamper() throws Exception {
		
		try (OutputStream out = NullOutputStream.NULL_OUTPUT_STREAM) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			
			// assert default mode is NONE
			assertThat(signParameter.getTSMode(), is(TSMode.NONE));
			
			ctrl.reset();
			
			// expect that timeStamper is ignored
			TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
			signParameter.setTimeStamper(timeStamper);
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			SignResult result = pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);
			
			ctrl.verify();
			
			assertFalse(result.isTimeStamped()); // tsMode: NONE
			
		}
		
	}
	
	@Test
	public void test_startExternalSignature_finishExternalSignature_tsMode_default_withoutTimeStamper() throws Exception {
		
		try (OutputStream out = NullOutputStream.NULL_OUTPUT_STREAM) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			
			// assert default mode is NONE
			assertThat(signParameter.getTSMode(), is(TSMode.NONE));
			
			ctrl.reset();

			// expect that a missing time stamper is not a problem
//			TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
//			signParameter.setTimeStamper(timeStamper);
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			SignResult result = pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);
			
			ctrl.verify();
			
			assertFalse(result.isTimeStamped());
			
		}
		
	}
	
	@Test
	public void test_startExternalSignature_finishExternalSignature_tsMode_OPTIONAL_withTimeStamper() throws Exception {
		
		try (OutputStream out = NullOutputStream.NULL_OUTPUT_STREAM) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setTSMode(TSMode.OPTIONAL);
			
			ctrl.reset();
			
			TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
			signParameter.setTimeStamper(timeStamper);
			
			Capture<byte[]> signatureValueCapture = newCapture();
			expect(timeStamper.applyTimeStamp(capture(signatureValueCapture))).andReturn(DerCoder.encode(new PrintableString("I'm a timestamp"))); // we need a valid ASN.1 object as timestamp otherwise ContentInfo/SignerInfo will not be updated
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			SignResult result = pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);
			
			ctrl.verify();
			
			assertTrue(result.isTimeStamped());
			
		}
		
	}
	
	@Test
	public void test_startExternalSignature_finishExternalSignature_tsMode_REQUIRED_withTimeStamper() throws Exception {
		
		try (OutputStream out = NullOutputStream.NULL_OUTPUT_STREAM) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setTSMode(TSMode.REQUIRED);
			
			ctrl.reset();
			
			TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
			signParameter.setTimeStamper(timeStamper);
			
			Capture<byte[]> signatureValueCapture = newCapture();
			expect(timeStamper.applyTimeStamp(capture(signatureValueCapture))).andReturn(DerCoder.encode(new PrintableString("I'm a timestamp"))); // we need a valid ASN.1 object as timestamp otherwise ContentInfo/SignerInfo will not be updated
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			SignResult result = pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);
			
			ctrl.verify();
			
			assertTrue(result.isTimeStamped());
			
		}
		
	}
	
	@Test
	public void test_startExternalSignature_finishExternalSignature_tsMode_OPTIONAL_withTimeStamper_failed() throws Exception {
		
		try (OutputStream out = NullOutputStream.NULL_OUTPUT_STREAM) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setTSMode(TSMode.OPTIONAL);
			
			ctrl.reset();
			
			TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
			signParameter.setTimeStamper(timeStamper);
			
			expect(timeStamper.applyTimeStamp(anyObject())).andThrow(new RuntimeException("timestamping failed")); // failed timestamping should not make signature fail
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			SignResult result = pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);
			
			ctrl.verify();
			
			assertFalse(result.isTimeStamped()); // timestamping failed
			
		}
		
	}
	
	@Test
	public void test_startExternalSignature_finishExternalSignature_tsMode_REQUIRED_withTimeStamper_failed() throws Exception {
		
		try (OutputStream out = NullOutputStream.NULL_OUTPUT_STREAM) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setTSMode(TSMode.REQUIRED);
			
			ctrl.reset();
			
			TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
			signParameter.setTimeStamper(timeStamper);
			
			expect(timeStamper.applyTimeStamp(anyObject())).andThrow(new RuntimeException("timestamping failed")); // failed timestamping should make signature fail
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			PDFASError error = assertThrows(PDFASError.class, () -> pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx));
			assertThat(error.getCode(), is(11200L)); // ERROR_SIG_TS_FAILED
			
			ctrl.verify();
			
		}
		
	}
	
	@Test
	public void test_startExternalSignature_finishExternalSignature_tsMode_REQUIRED_withoutTimeStamper() throws Exception {
		
		try (OutputStream out = NullOutputStream.NULL_OUTPUT_STREAM) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			
			ctrl.reset();
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();

			// set parameters after startSignature
			signParameter.setTSMode(TSMode.REQUIRED);

			PDFASError error = assertThrows(PDFASError.class, () -> pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx));
			assertThat(error.getCode(), is(11201L)); // ERROR_SIG_TS_FAILED
			
			ctrl.verify();
			
		}
		
	}
	
	@Test
	public void test_startExternalSignature_tsMode_REQUIRED_withoutTimeStamper() throws Exception {
		
		try (OutputStream out = NullOutputStream.NULL_OUTPUT_STREAM) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setTSMode(TSMode.REQUIRED);
			
			ctrl.reset();
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			PDFASError error = assertThrows(PDFASError.class, () -> pdfas.startExternalSignature(signParameter, signingCertificate, ctx));
			assertThat(error.getCode(), is(11201L)); // ERROR_SIG_TS_TSMODE_REQUIRES_TIMESTAMPER
			
			ctrl.verify();
			
		}
		
	}
	
	@Test
	public void test_startExternalSignature_finishExternalSignature_tsMode_OPTIONAL_withoutTimeStamper() throws Exception {

		try (OutputStream out = NullOutputStream.NULL_OUTPUT_STREAM) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setTSMode(TSMode.OPTIONAL);
			
			ctrl.reset();
			
			// using TSMode.OPTIONAL but not setting a timeStamper does not make sens, but should be allowed and should not abort signature
//			TimeStamper timeStamper = ctrl.createMock(TimeStamper.class);
//			signParameter.setTimeStamper(timeStamper);
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			SignResult result = pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);
			
			ctrl.verify();
			
			assertFalse(result.isTimeStamped()); // timestamper not set
			
		}

	}
	
	@Test
	public void test_finishExternalSignature_invalidSignatureValue() throws Exception {

		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(new PAdESExternalSigner());
		
		ctrl.reset();
		
		ctrl.replay();
		
		ExternalSignatureContext ctx = new ExternalSignatureContext();
		
		pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
		
		// ** create external signature (from content != inputDataSource)
		Signature signature = Signature.getInstance("NONEwithECDSA");
		signature.initSign(signingKey);
		
		// deliberately calculate digest from wrong content
		byte[] digestInputData = "otherContent".getBytes();
		byte[] digest = new AlgorithmID(new ObjectID(ctx.getDigestAlgorithmOid())).getMessageDigestInstance().digest(digestInputData);
		
		signature.update(digest);
		byte[] externalSignatureValue = signature.sign();
		
		// expect that finishSignature detects invalid signature (value)
		
		PDFASError ex = assertThrows(PDFASError.class, () -> pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx));
		assertThat(ex.getCode(), is(11008L));  // at.gv.egiz.pdfas.common.exceptions.ErrorConstants.ERROR_SIG_INVALID_BKU_SIG
		assertThat(ex.getCause(), is(instanceOf(InvalidSignatureValueException.class)));
		
		ctrl.verify();

	}
	
	@Test
	public void test_validate_externalSignatureContext() throws NoSuchAlgorithmException {
		
		ExternalSignatureContext ctx = new ExternalSignatureContext();
		
		// set all values -> valid
		ctx.setDigestAlgorithmOid("2.16.840.1.101.3.4.2.1");
		ctx.setDigestValue(MessageDigest.getInstance("SHA-256").digest("hello world".getBytes()));
		ctx.setPreparedDocument(new ByteArrayDataSource(new byte[] { 1, 2, 3 }));
		ctx.setSignatureAlgorithmOid("1.2.840.10045.4.3.2");
		ctx.setSignatureByteRange(new int[] { 0, 87875, 96069, 1838 });
		ctx.setSignatureObject(new byte[] { 1, 2, 3 });
		ctx.setSigningCertificate(signingCertificate);
		ctx.setSigningTime(Calendar.getInstance());
		
		PdfAsImpl.validate(ctx);
		
		ctx.setSigningTime(null);
		PdfAsImpl.validate(ctx);
		
		assertThrows(NullPointerException.class, () -> PdfAsImpl.validate(null));
		
		ctx.setDigestAlgorithmOid(null);
		assertThrows(IllegalArgumentException.class, () -> PdfAsImpl.validate(ctx));
		ctx.setDigestAlgorithmOid("2.16.840.1.101.3.4.2.1");
		
		ctx.setDigestValue(null);
		assertThrows(IllegalArgumentException.class, () -> PdfAsImpl.validate(ctx));
		ctx.setDigestValue(MessageDigest.getInstance("SHA-256").digest("hello world".getBytes()));
		
		ctx.setPreparedDocument(null);
		assertThrows(IllegalArgumentException.class, () -> PdfAsImpl.validate(ctx));
		ctx.setPreparedDocument(new ByteArrayDataSource(new byte[] { 1, 2, 3 }));		

		ctx.setSignatureAlgorithmOid(null);
		assertThrows(IllegalArgumentException.class, () -> PdfAsImpl.validate(ctx));
		ctx.setSignatureAlgorithmOid("1.2.840.10045.4.3.2");
		
		ctx.setSignatureByteRange(null);
		assertThrows(IllegalArgumentException.class, () -> PdfAsImpl.validate(ctx));
		ctx.setSignatureByteRange(new int[] { });
		assertThrows(IllegalArgumentException.class, () -> PdfAsImpl.validate(ctx));
		ctx.setSignatureByteRange(new int[] { 1, 2, 3 });
		assertThrows(IllegalArgumentException.class, () -> PdfAsImpl.validate(ctx));
		ctx.setSignatureByteRange(new int[] { 0, 87875, 96069, 1838 });

		ctx.setSignatureObject(null);
		assertThrows(IllegalArgumentException.class, () -> PdfAsImpl.validate(ctx));
		ctx.setSignatureObject(new byte[] { 1, 2, 3 });

		ctx.setSigningCertificate(null);
		assertThrows(IllegalArgumentException.class, () -> PdfAsImpl.validate(ctx));
		
	}
	
	@Test
	public void test_verifyTSParameters() throws PDFASError {

		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		
		// neutral default settings (tsMode=NONE, timeStamper=null)
		PdfAsImpl.verifyTSParameters(signParameter);

		// tsMode=OPTIONAL, timeStamper=null: accepted but with INFO log
		signParameter.setTSMode(TSMode.OPTIONAL);
		PdfAsImpl.verifyTSParameters(signParameter);
		
		// tsMode=REQUIRED, timeStamper=null: validation fails with error 11201
		signParameter.setTSMode(TSMode.REQUIRED);
		PDFASError error = assertThrows(PDFASError.class, () -> PdfAsImpl.verifyTSParameters(signParameter));
		assertThat(error.getCode(), is(11201L));
		
		// tsMode=REQUIRED, timeStamper!=null: accepted
		signParameter.setTSMode(TSMode.REQUIRED);
		signParameter.setTimeStamper(signatureValue -> null);  // we do not care that timestamper always returns null
		PdfAsImpl.verifyTSParameters(signParameter);
		
		// tsMode=NONE, timeStamper!=null: accepted
		signParameter.setTSMode(TSMode.NONE);
		signParameter.setTimeStamper(signatureValue -> null);  // we do not care that timestamper always returns null
		PdfAsImpl.verifyTSParameters(signParameter);
		
		// tsMode=OPTIONAL, timeStamper!=null: accepted
		signParameter.setTSMode(TSMode.OPTIONAL);
		signParameter.setTimeStamper(signatureValue -> null);  // we do not care that timestamper always returns null
		PdfAsImpl.verifyTSParameters(signParameter);

	}
	
	@Test
	public void test_writeExternalSignature() throws IOException {
		
		byte[] preparedDocument = new byte[1000]; // 1000 bytes
		Arrays.fill(preparedDocument, (byte) '.');
		preparedDocument[100] = '<';
		preparedDocument[499] = '>';
		
		byte[] cmsSignature = new byte[100]; // 100 bytes
		new Random().nextBytes(cmsSignature);
		
		byte[] pdfSignature = Hex.encodeHexString(cmsSignature, false).getBytes(); // 200 bytes

		// reserved space: bytes [ 101...499 ] // 398 bytes available for pdfSignature (398/2 = 199 bytes for cmsSignature)
		int[] byteRange = new int[] { 0, 100, 500, 500 };
		
		ExternalSignatureContext ctx = new ExternalSignatureContext();
		ctx.setSignatureByteRange(byteRange);
		ctx.setPreparedDocument(new ByteArrayDataSource(preparedDocument));
		
		byte[] result;
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			PdfAsImpl.writeExternalSignature(ctx, pdfSignature, out);
			result = out.toByteArray();
		}
		
		// examine result (signed document)
		// pdf data consists of "."
		// pdf signature consists of hex digits
		// remaining reserved space is filled with character '0'
		
		HexDump.dump(result, 0, System.out, 0);
		
		// length of document did not change
		assertThat(result.length, is(preparedDocument.length));
		// pdf signature has been inserted
		assertThat(Arrays.copyOfRange(result, 101, 301), is(pdfSignature));
		
		// make sure the remaining bytes of the reserved spaced are padded with '0'
		byte[] padding = new byte[398-200];
		Arrays.fill(padding, (byte) '0');
		assertThat(Arrays.copyOfRange(result, 301, 499), is(padding));
		
		// make sure '<' and '>' remain unmodified
		assertThat(result[100], is((byte) '<'));
		assertThat(result[499], is((byte) '>'));

	}
	
	@Test
	public void test_writeExternalSignature_use_space_completely() throws IOException {
		
		byte[] preparedDocument = new byte[1000]; // 1000 bytes
		Arrays.fill(preparedDocument, (byte) '.');
		preparedDocument[100] = '<';
		preparedDocument[499] = '>';
		
		byte[] cmsSignature = new byte[199]; // 199 bytes
		new Random().nextBytes(cmsSignature);
		
		byte[] pdfSignature = Hex.encodeHexString(cmsSignature, false).getBytes(); // 398 bytes

		// reserved space: bytes [ 101...498 ] // 398 bytes available for pdfSignature (398/2 = 199 bytes for cmsSignature)
		int[] byteRange = new int[] { 0, 100, 500, 500 };
		
		ExternalSignatureContext ctx = new ExternalSignatureContext();
		ctx.setSignatureByteRange(byteRange);
		ctx.setPreparedDocument(new ByteArrayDataSource(preparedDocument));
		
		byte[] result;
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			PdfAsImpl.writeExternalSignature(ctx, pdfSignature, out);
			result = out.toByteArray();
		}
		
		// examine result (signed document)
		// pdf data consists of "."
		// pdf signature consists of hex digits
		// remaining reserved space is filled with character '0'
		
		HexDump.dump(result, 0, System.out, 0);
		
		// length of document did not change
		assertThat(result.length, is(preparedDocument.length));
		// pdf signature has been inserted
		assertThat(Arrays.copyOfRange(result, 101, 499), is(pdfSignature));
		
		// make sure '<' and '>' remain unmodified
		assertThat(result[100], is((byte) '<'));
		assertThat(result[499], is((byte) '>'));
		
	}
	
	@Test
	public void test_writeExternalSignature_insufficient_space() throws IOException {
		
		byte[] preparedDocument = new byte[1000]; // 1000 bytes
		Arrays.fill(preparedDocument, (byte) '.');
		preparedDocument[100] = '<';
		preparedDocument[499] = '>';
		
		byte[] cmsSignature = new byte[200]; // 200 bytes
		new Random().nextBytes(cmsSignature);
		
		byte[] pdfSignature = Hex.encodeHexString(cmsSignature, false).getBytes(); // 400 bytes

		// reserved space: bytes [ 101...498 ] // 398 bytes available for pdfSignature (398/2 = 199 bytes for cmsSignature)
		int[] byteRange = new int[] { 0, 100, 500, 500 };
		
		ExternalSignatureContext ctx = new ExternalSignatureContext();
		ctx.setSignatureByteRange(byteRange);
		ctx.setPreparedDocument(new ByteArrayDataSource(preparedDocument));
		
		IOException e = assertThrows(IOException.class, () -> PdfAsImpl.writeExternalSignature(ctx, pdfSignature, NullOutputStream.NULL_OUTPUT_STREAM));
		System.out.println(e.getMessage());

	}
	
	private void validateCertificationLevel(@WillNotClose @Nonnull PDDocument doc, int certificationLevel) throws IOException {
		
		// test if certification signature is present
		List<PDSignature> sigDicts = doc.getSignatureDictionaries();
		assertThat(sigDicts, hasSize(1));
		
		PDSignature signature = sigDicts.get(0);
		
		COSDictionary permDict = (COSDictionary) doc.getDocumentCatalog().getCOSObject().getDictionaryObject(COSName.PERMS);
		COSObject docMDPValue = permDict.getCOSObject(COSName.DOCMDP);
		// make sure DOCMDP entry references the signature
		assertThat(docMDPValue.getObject(), is(signature.getCOSObject()));
		
		COSObject referenceDictObject = (COSObject) signature.getCOSObject().getCOSArray(COSName.REFERENCE).get(0).getCOSObject();
		COSDictionary referenceDict = (COSDictionary) referenceDictObject.getObject();
		assertThat(referenceDict.getCOSName(COSName.TRANSFORM_METHOD), is(COSName.DOCMDP));
		
		COSDictionary transformParams = referenceDict.getCOSDictionary(COSName.TRANSFORM_PARAMS);
		
		assertThat(transformParams.getInt(COSName.P), is(certificationLevel)); // ISO 32000-1:2008, table 254
		
	}

	@Test
	public void test_sign_certified_prevent_with_existing_signatures() throws PDFASError, IOException {
		
		DataSource signedFileDataSource;
		try (InputStream in = PdfAsImplTest.class.getResourceAsStream("/approved_signature.pdf")) {
			signedFileDataSource = new ByteArrayDataSource(IOUtils.toByteArray(in));
		}
		
		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), signedFileDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		
		// request certification signature
		signParameter.setSignatureCertificationLevel(SignParameter.CERTIFIED_NO_CHANGES_ALLOWED);
		
		PDFASError e = assertThrows(PDFASError.class, () -> pdfas.sign(signParameter));
		assertThat(e.getCode(), is(11050L));  // at.gv.egiz.pdfas.common.exceptions.ErrorConstants.ERROR_SIG_CERTIFICATION_SIGNATURES_NOT_ALLOWED_ON_SIGNED_DOCUMENTS
		
	}
	
	@Test
	public void test_sign_certified_CERTIFIED_NO_CHANGES_ALLOWED() throws PDFASError, IOException {
		
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(signer);
			
			// request certification signature
			signParameter.setSignatureCertificationLevel(SignParameter.CERTIFIED_NO_CHANGES_ALLOWED);
			
			pdfas.sign(signParameter);
			
			out.flush();
			
			try (PDDocument doc = PDDocument.load(out.toByteArray())) {
				validateCertificationLevel(doc, 1); // ISO 32000-1:2008, table 254 (no changes allowed)
			}
			
		}
		
	}
	
	@Test
	public void test_sign_certified_CERTIFIED_FORM_FILLING() throws PDFASError, IOException {
		
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(signer);
			
			// request certification signature
			signParameter.setSignatureCertificationLevel(SignParameter.CERTIFIED_FORM_FILLING);
			
			pdfas.sign(signParameter);
			
			out.flush();
			
			try (PDDocument doc = PDDocument.load(out.toByteArray())) {
				validateCertificationLevel(doc, 2); // ISO 32000-1:2008, table 254 (form filling allowed)
			}
			
		}
		
	}

	@Test
	public void test_sign_certified_CERTIFIED_FORM_FILLING_AND_ANNOTATIONS() throws PDFASError, IOException {
		
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(signer);
			
			// request certification signature
			signParameter.setSignatureCertificationLevel(SignParameter.CERTIFIED_FORM_FILLING_AND_ANNOTATIONS);
			
			pdfas.sign(signParameter);
			
			out.flush();
			
			try (PDDocument doc = PDDocument.load(out.toByteArray())) {
				validateCertificationLevel(doc, 3); // ISO 32000-1:2008, table 254 (form filling and annotations allowed)
			}
			
		}
		
	}
	
	@Test
	public void test_sign_certified_NOT_CERTIFIED() throws PDFASError, IOException {
		
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(signer);
			
			// request certification signature
			signParameter.setSignatureCertificationLevel(SignParameter.NOT_CERTIFIED);
			
			pdfas.sign(signParameter);
			
			out.flush();
			
			try (PDDocument doc = PDDocument.load(out.toByteArray())) {
				
				// test if certification signature is present
				List<PDSignature> sigDicts = doc.getSignatureDictionaries();
				assertThat(sigDicts, hasSize(1));
				
				PDSignature signature = sigDicts.get(0);

				assertNull(doc.getDocumentCatalog().getCOSObject().getDictionaryObject(COSName.PERMS));
				assertNull(signature.getCOSObject().getCOSArray(COSName.REFERENCE));
				
			}
			
		}
		
	}

	@Test
	public void test_startExternalSignature_finishExternalSignature_certified_CERTIFIED_NO_CHANGES_ALLOWED() throws Exception {

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setSignaturePosition("p:1;x:2;y:3");
			signParameter.setSignatureProfileId("SIGNATURBLOCK_DE");
			signParameter.setSignatureCertificationLevel(SignParameter.CERTIFIED_NO_CHANGES_ALLOWED);
			
			ctrl.reset();
			
			SigningTimeSource signingTimeSource = ctrl.createMock(SigningTimeSource.class);
			signParameter.setSigningTimeSource(signingTimeSource);
			
			Calendar signingTime = toCalendar("2007-12-03T10:15:30.00Z");
			
			expect(signingTimeSource.getSigningTime(anyObject(RequestedSignature.class))).andReturn(signingTime);
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);
			
			ctrl.verify();
			
			try (PDDocument doc = PDDocument.load(out.toByteArray())) {
				validateCertificationLevel(doc, 1); // ISO 32000-1:2008, table 254 (no changes allowed)
			}

		}

	}

	@Test
	public void test_startExternalSignature_finishExternalSignature_certified_CERTIFIED_FORM_FILLING() throws Exception {

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setSignaturePosition("p:1;x:2;y:3");
			signParameter.setSignatureProfileId("SIGNATURBLOCK_DE");
			signParameter.setSignatureCertificationLevel(SignParameter.CERTIFIED_FORM_FILLING);
			
			ctrl.reset();
			
			SigningTimeSource signingTimeSource = ctrl.createMock(SigningTimeSource.class);
			signParameter.setSigningTimeSource(signingTimeSource);
			
			Calendar signingTime = toCalendar("2007-12-03T10:15:30.00Z");
			
			expect(signingTimeSource.getSigningTime(anyObject(RequestedSignature.class))).andReturn(signingTime);
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);
			
			ctrl.verify();
			
			try (PDDocument doc = PDDocument.load(out.toByteArray())) {
				validateCertificationLevel(doc, 2); // ISO 32000-1:2008, table 254 (form filling allowed)
			}

		}

	}
	
	@Test
	public void test_startExternalSignature_finishExternalSignature_certified_NOT_CERTIFIED() throws Exception {
		
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setSignaturePosition("p:1;x:2;y:3");
			signParameter.setSignatureProfileId("SIGNATURBLOCK_DE");
			signParameter.setSignatureCertificationLevel(SignParameter.NOT_CERTIFIED);
			
			ctrl.reset();
			
			SigningTimeSource signingTimeSource = ctrl.createMock(SigningTimeSource.class);
			signParameter.setSigningTimeSource(signingTimeSource);
			
			Calendar signingTime = toCalendar("2007-12-03T10:15:30.00Z");
			
			expect(signingTimeSource.getSigningTime(anyObject(RequestedSignature.class))).andReturn(signingTime);
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);

			ctrl.verify();
			
			try (PDDocument doc = PDDocument.load(out.toByteArray())) {

				// test if certification signature is present
				List<PDSignature> sigDicts = doc.getSignatureDictionaries();
				assertThat(sigDicts, hasSize(1));
				
				PDSignature pdSignature = sigDicts.get(0);

				assertNull(doc.getDocumentCatalog().getCOSObject().getDictionaryObject(COSName.PERMS));
				assertNull(pdSignature.getCOSObject().getCOSArray(COSName.REFERENCE));

			}
			
		}
		
	}

	@Test
	public void test_startExternalSignature_finishExternalSignature_certified_prevent_with_existing_signatures() throws Exception {
		
		DataSource signedFileDataSource;
		try (InputStream in = PdfAsImplTest.class.getResourceAsStream("/approved_signature.pdf")) {
			signedFileDataSource = new ByteArrayDataSource(IOUtils.toByteArray(in));
		}

		SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), signedFileDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(new PAdESExternalSigner());
		signParameter.setSignaturePosition("p:1;x:2;y:3");
		signParameter.setSignatureProfileId("SIGNATURBLOCK_DE");
		signParameter.setSignatureCertificationLevel(SignParameter.CERTIFIED_NO_CHANGES_ALLOWED);

		
		ExternalSignatureContext ctx = new ExternalSignatureContext();
		
		PDFASError e = assertThrows(PDFASError.class, () -> pdfas.startExternalSignature(signParameter, signingCertificate, ctx));
		assertThat(e.getCode(), is(11050L));  // at.gv.egiz.pdfas.common.exceptions.ErrorConstants.ERROR_SIG_CERTIFICATION_SIGNATURES_NOT_ALLOWED_ON_SIGNED_DOCUMENTS
		
	}

	@Test
	public void test_startExternalSignature_finishExternalSignature_certified_CERTIFIED_FORM_FILLING_AND_ANNOTATIONS() throws Exception {

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			
			SignParameter signParameter = PdfAsFactory.createSignParameter(pdfas.getConfiguration(), inputDataSource, out);
			signParameter.setPlainSigner(new PAdESExternalSigner());
			signParameter.setSignaturePosition("p:1;x:2;y:3");
			signParameter.setSignatureProfileId("SIGNATURBLOCK_DE");
			signParameter.setSignatureCertificationLevel(SignParameter.CERTIFIED_FORM_FILLING_AND_ANNOTATIONS);
			
			ctrl.reset();
			
			SigningTimeSource signingTimeSource = ctrl.createMock(SigningTimeSource.class);
			signParameter.setSigningTimeSource(signingTimeSource);
			
			Calendar signingTime = toCalendar("2007-12-03T10:15:30.00Z");
			
			expect(signingTimeSource.getSigningTime(anyObject(RequestedSignature.class))).andReturn(signingTime);
			
			ctrl.replay();
			
			ExternalSignatureContext ctx = new ExternalSignatureContext();
			
			pdfas.startExternalSignature(signParameter, signingCertificate, ctx);
			
			// ** create external signature
			Signature signature = Signature.getInstance("NONEwithECDSA");
			signature.initSign(signingKey);
			signature.update(ctx.getDigestValue());
			byte[] externalSignatureValue = signature.sign();
			
			pdfas.finishExternalSignature(signParameter, externalSignatureValue, ctx);
			
			ctrl.verify();
			
			try (PDDocument doc = PDDocument.load(out.toByteArray())) {
				validateCertificationLevel(doc, 3); // ISO 32000-1:2008, table 254 (form filling and annotations allowed)
			}

		}

	}

	@Test
	public void test_sign_illegalParameter() throws PDFASError, NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		
		Configuration configuration = pdfas.getConfiguration();
		SignParameter signParameter = PdfAsFactory.createSignParameter(configuration, inputDataSource, NullOutputStream.NULL_OUTPUT_STREAM);
		signParameter.setPlainSigner(signer);
		
		configuration.setValue("sig_obj.SIGNATURBLOCK_DE.value.SIG_DATE", "invalidPattern");
		
		signParameter.setSignatureProfileId("SIGNATURBLOCK_DE");
		
		PDFASError e = assertThrows(PDFASError.class, () -> pdfas.sign(signParameter));
		assertThat(e.getCode(), is(11021L));
		
	}
	
}
